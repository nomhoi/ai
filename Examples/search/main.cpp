#include "../../Search/Search.h"
#include <iostream>
#include <vector>
#include <cstring>

using namespace std;
using namespace ai;

const int Up = 0;
const int Down = 1;
const int Left = 2;
const int Right = 3;

const int size = 3;

typedef vector<int> Board;
typedef int Action;

template <class TState, class TAction> class ConcreteState : public StateNode<TState, TAction> {
public:
    vector<int> actions = {Up, Down, Left, Right};
    int i, j, k;

    ConcreteState()
    {
        StateNode<TState, TAction>::state = {0,1,2,3,4,5,6,7,8};
    }

    ConcreteState(StateNode<TState, TAction>* stateNode)
    {
        StateNode<TState, TAction>::state = stateNode->state;
    }

    ConcreteState(char* _board)
    {
        char* chars_array = strtok(_board, ",");
        while (chars_array != NULL) {
            StateNode<TState, TAction>::state.push_back(atoi(chars_array));
            chars_array = strtok (NULL, ",");
        }

        k = 0;
        for (auto cell: StateNode<TState, TAction>::state) {
            if (cell == 0) {
                i = int(k / size);
                j = k - i * size;
                return;
            }
            k++;
        }

        StateNode<TState, TAction>::action = 0;
    }

    ConcreteState* createNewState(const int& ni, const int& nj, const int& action)
    {
        ConcreteState* stateNode = new ConcreteState(this);

        int nk = ni * size + nj;
        stateNode->state[k] = stateNode->state[nk];
        stateNode->state[nk] = 0;
        stateNode->k = nk;
        stateNode->i = ni;
        stateNode->j = nj;
        stateNode->action = action;
        stateNode->parentStateNode = this;

        //cout << "createNewState : " << action << endl;
        //newState->print();

        return stateNode;
    }

    virtual list<StateNode<TState, TAction>*> successors()
    {
        //print();
        list<StateNode<TState, TAction>*> res;

        for (auto action: actions) {
            if (action == Up && i > 0)
                res.push_back(createNewState(i-1, j, Up));
            else if (action == Down && i < size - 1)
                res.push_back(createNewState(i+1, j, Down));
            else if (action == Left && j > 0)
                res.push_back(createNewState(i, j-1, Left));
            else if (action == Right && j < size - 1)
                res.push_back(createNewState(i, j+1, Right));
        }

        return res;
    }

    void print()
    {
        cout << ": ";
        for (auto state: StateNode<TState, TAction>::state)
            cout << state << " ";
        cout << endl;
    }

};

template <class TAction>
void printActions(list<TAction> actions)
{
    cout << "actions:";
    for (auto action: actions) {
        switch (action) {
        case Up:
            cout << " " << "Up";
            break;
        case Down:
            cout << " " << "Down";
            break;
        case Left:
            cout << " " << "Left";
            break;
        case Right:
            cout << " " << "Right";
            break;
        default:
            break;
        }

    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    ConcreteState<Board, Action> initialStateNode(argv[2]);
    ConcreteState<Board, Action> goalStateNode;
    BreadthFirstSearch<Board, Action> bfs;
    bfs.initialStateNode = &initialStateNode;
    bfs.goalStateNode = &goalStateNode;
    if (bfs.search())
        printActions(bfs.actions());

    DepthFirstSearch<Board, Action> dfs;
    dfs.initialStateNode = &initialStateNode;
    dfs.goalStateNode = &goalStateNode;
    if (dfs.search())
        printActions(dfs.actions());

    DepthLimitedSearch<Board, Action> dls;
    dls.initialStateNode = &initialStateNode;
    dls.goalStateNode = &goalStateNode;
    dls.depth = 4;
    if (dls.search())
        printActions(dls.actions());

    IterativeDeepeningSearch<Board, Action> ids;
    ids.initialStateNode = &initialStateNode;
    ids.goalStateNode = &goalStateNode;
    if (ids.search())
        printActions(ids.actions());

    return 0;
}
