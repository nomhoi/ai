import sys
import os
import math
import time
import collections
import queue
from heapq import heappush, heappop, heapify

class MyPriorityQueue(queue.Queue):
    def _init(self, maxsize):
        self.queue = []

    def _qsize(self):
        return len(self.queue)

    def _put(self, item):
        heappush(self.queue, item)

    def _get(self):
        return heappop(self.queue)

    def decreaseKey(self, item):
        for index, (p, i) in enumerate(self.queue):
            if i == item[1]:
                if p <= item[0]:
                    break
                del self.queue[index]
                self.queue.append(item)
                heapify(self.queue)
                break
        else:
            self.put(item)
            return True
        return False

def memory():
    if os.name == 'posix':
        import resource
        return float(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024)
    else:
        import psutil
        p = psutil.Process()
        return float(p.memory_info().rss / 1048576)

explored = set()
frontier = collections.deque()
frontier_set = set()
frontier_pq = MyPriorityQueue()
goal = None
length = 0
size = 0
max_depth = 0
initial_state = None
r, c = 0, 0 # row, col
a = 0 # action
d = 0 # depth
p = 0 # parent
cost = 0 # cost

def initState(board):
    data = list(map(lambda x: int(x), board.split(',')))
    global length
    length = len(data)
    global size
    size = int(math.sqrt(len(data)))
    #print(size, data)

    i, j = -1, -1
    for x0 in range(0, size):
        for y0 in range(0, size):
            k = x0 * size + y0
            if data[k] == 0:
                i, j = x0, y0
                break

    l = data[:]
    l.extend([i, j, 0, 0, None, 0])
    global initial_state
    initial_state = tuple(l)
    #print('state:', state)

    global goal
    goal = tuple(range(0, len(data)))
    #print('goal:', goal)
    global r, c, a, d, p, cost
    r = length
    c = length + 1
    a = length + 2
    d = length + 3
    p = length + 4
    cost = length + 5

def isGoalState(state):
    global goal
    return goal == state

def newStateData(state, i, j, ni, nj, action):
    global size, r, c, a, d, p, length
    k = i * size + j
    nk = ni * size + nj

    newstate = list(state)
    newstate[k] = newstate[nk]
    newstate[nk] = 0
    newstate[r] = ni
    newstate[c] = nj
    newstate[a] = action
    newstate[d] = state[d] + 1
    newstate[p] = state

    global frontier, frontier_set, explored
    nstate = tuple(newstate)
    if nstate[0:length] not in explored:
        if nstate[0:length] not in frontier_set:
            frontier.append(nstate)
            frontier_set.add(nstate[0:length])

            global max_depth
            if max_depth < nstate[d]:
                max_depth = nstate[d]

            #print(frontier)

def getSuccessors(state, reversed, newStateData):
    global size, r, c, d, p
    i, j = state[r], state[c]
    actions = []
    if reversed:
        actions = ['Right', 'Left', 'Down', 'Up']
    else:
        actions = ['Up', 'Down', 'Left', 'Right']

    for action in actions:
        #print(action)
        if action == 'Up' and i > 0:
            newStateData(state, i, j, i-1, j, 0)
        elif action == 'Down' and i < size - 1:
            newStateData(state, i, j, i+1, j, 1)
        elif action == 'Left' and j > 0:
            newStateData(state, i, j, i, j-1, 2)
        elif action == 'Right' and j  < size - 1:
            newStateData(state, i, j, i, j+1, 3)

def h(data):
    from functools import reduce
    global size, length
    distance = 0
    for i in range(0, length):
        r = data[i]
        distance += (abs(int(i / size) - int(r / size)) +abs(i % size - r % size))
    return distance

def newStateDataA(state, i, j, ni, nj, action):
    global size, r, c, a, d, p, length, cost
    k = i * size + j
    nk = ni * size + nj

    newstate = list(state)
    newstate[k] = newstate[nk]
    newstate[nk] = 0
    newstate[r] = ni
    newstate[c] = nj
    newstate[a] = action
    newstate[d] = state[d] + 1
    newstate[p] = state
    newstate[cost] = state[cost] + 1

    global frontier_pq, frontier_dict, explored
    nstate = tuple(newstate)
    priority = newstate[cost] + h(nstate[0:length])

    print('g + h:', newstate[cost], h(nstate[0:length]), priority)

    if nstate[0:length] not in explored:
        if frontier_pq.decreaseKey((priority, nstate[0:length])):
            frontier_dict[nstate[0:length]] = nstate
        global max_depth
        if max_depth < nstate[d]:
            max_depth = nstate[d]

def getActions(state):
    r = state
    actions = [state[a]]
    while r[p] is not None:
        r = r[p]
        if r[p] is not None:
            actions.append(r[a])
            #print(actions, r)

    #print(actions)
    #return []
    return actions[::-1]

def write_result(state, nodes_expanded, frontier_size, max_f):
    global start
    global max_depth
    t = time.time() - start
    #print('time:', t)

    actions = []
    for a in getActions(state):
        if a == 0:
            actions.append('Up')
        elif a == 1:
            actions.append('Down')
        elif a == 2:
            actions.append('Left')
        elif a == 3:
            actions.append('Right')

    output = 'path_to_goal: {0}\ncost_of_path: {1}\nnodes_expanded: {2}\nfringe_size: {3}\nmax_fringe_size: {4}\nsearch_depth: {5}\nmax_search_depth: {6}\nrunning_time: {7:.8f}\nmax_ram_usage: {8:.8f}\n'\
            .format(actions, len(actions), nodes_expanded, frontier_size, max_f, state[d], max_depth, t, memory())
    '''
    if len(actions) > 20:
        output2 = 'path_to_goal(more then 20): {0}\ncost_of_path: {1}\nnodes_expanded: {2}\nfringe_size: {3}\nmax_fringe_size: {4}\nsearch_depth: {5}\nmax_search_depth: {6}\nrunning_time: {7:.8f}\nmax_ram_usage: {8:.8f}\n'\
                .format(actions[:20], len(actions), nodes_expanded, frontier_size, max_f, state[d], max_depth, t, memory())
        print(output2)
    else:
    '''
    print(output)
    f = open('output.txt', 'w')
    f.write(output)
    f.close()

def breadthFirstSearch():
    global initial_state
    global frontier
    global explored
    frontier.append(initial_state)
    frontier_set.add(initial_state[0:length])
    max_f = len(frontier)

    while frontier:
        if len(frontier) > max_f:
            max_f = len(frontier)
        state = frontier.popleft()
        frontier_set.remove(state[0:length])
        #print('state:', state[:-1])
        explored.add(state[0:length])
        #print('explored:', explored)

        if isGoalState(state[0:length]):
            write_result(state,  len(explored) - 1, len(frontier), max_f)
            return True
        getSuccessors(state, False, newStateData)

    return False

def depthFirstSearch():
    global initial_state
    global frontier
    global explored
    frontier.append(initial_state)
    frontier_set.add(initial_state[0:length])
    max_f = len(frontier)

    while frontier:
        if len(frontier) > max_f:
            max_f = len(frontier)
        state = frontier.pop()
        #print('test', state[0:length])
        #print('frontier_set', frontier_set)
        frontier_set.remove(state[0:length])
        #print('state:', state[:-1])
        explored.add(state[0:length])
        #print('explored:', explored)

        if isGoalState(state[0:length]):
            write_result(state,  len(explored) - 1, len(frontier), max_f)
            return True
        getSuccessors(state, True, newStateData)

    return False

def depthLimitedSearch(depth):
    global initial_state
    global frontier
    global explored
    frontier.append(initial_state)
    frontier_set.add(initial_state[0:length])
    max_f = len(frontier)
    while frontier:
        if len(frontier) > max_f:
            max_f = len(frontier)
        state = frontier.pop()
        if state[d] > depth:
            continue
        frontier_set.remove(state[0:length])
        explored.add(state[0:length])
        if isGoalState(state[0:length]):
            write_result(state,  len(explored) - 1, len(frontier), max_f)
            return True
        getSuccessors(state, True, newStateData)

    return False

def iterativeDeepeningSearch():
    global frontier, explored, frontier_set
    depth = 1
    while True:
        if depthLimitedSearch(depth):
            return True
        depth += 1
        frontier.clear()
        explored.clear()
        frontier_set.clear()
    return False

frontier_dict = dict()

def aStarSearch():
    global initial_state
    global frontier_pq, frontier_dict
    global explored
    frontier_pq.put((0, initial_state[0:length]))
    frontier_dict[initial_state[0:length]] = initial_state
    max_f = frontier_pq.qsize()
    #print(max_f)

    while not frontier_pq.empty():
        if frontier_pq.qsize() > max_f:
            max_f = frontier_pq.qsize()
        state = frontier_pq.get()
        #del frontier_dict[state]
        #print('state:', state)

        #frontier_set.remove(state[0:length])
        #print('state:', state[:-1])
        explored.add(state[1])
        #print('explored:', explored)

        if isGoalState(state[1]):
            write_result(frontier_dict[state[1]],  len(explored) - 1, frontier_pq.qsize(), max_f)
            return True

        #print(frontier_dict[state])
        getSuccessors(frontier_dict[state[1]], True, newStateDataA)
        del frontier_dict[state[1]]

    return False

min = sys.maxsize
tr = 0

def newStateDataIDA(state, i, j, ni, nj, action):
    global size, r, c, a, d, p, length
    k = i * size + j
    nk = ni * size + nj

    newstate = list(state)
    newstate[k] = newstate[nk]
    newstate[nk] = 0
    newstate[r] = ni
    newstate[c] = nj
    newstate[a] = action
    newstate[d] = state[d] + 1
    newstate[p] = state
    newstate[cost] = state[cost] + 1
    global frontier, frontier_set, explored
    nstate = tuple(newstate)
    f = newstate[cost] + h(nstate[0:length])
    global tr, min
    if f < min and f > tr:
        min = f
    if f <= tr:
        if nstate[0:length] not in explored:
            if nstate[0:length] not in frontier_set:
                frontier.append(nstate)
                frontier_set.add(nstate[0:length])
                global max_depth
                if max_depth < nstate[d]:
                    max_depth = nstate[d]

def search(node, g, threshold):
    #print('threshold:', threshold)
    global frontier, explored, min, tr
    min = sys.maxsize
    frontier.append(node)
    frontier_set.add(node[0:length])
    max_f = len(frontier)
    while frontier:
        if len(frontier) > max_f:
            max_f = len(frontier)
        state = frontier.pop()
        frontier_set.remove(state[0:length])
        explored.add(state[0:length])
        if isGoalState(state[0:length]):
            write_result(state,  len(explored) - 1, len(frontier), max_f)
            return True, threshold
        tr = threshold
        getSuccessors(state, True, newStateDataIDA)

    return False, min

def idaSearch():
    global initial_state, frontier, explored, frontier_set
    threshold = h(initial_state[0:length])
    #print('threshold:', threshold)
    while True:
        found, temp = search(initial_state, 0, threshold)
        if found:
            return True
        if temp == sys.maxsize:
            return False
        threshold = temp
        frontier.clear()
        explored.clear()
        frontier_set.clear()
    return False

def main(argv):
    global start
    method = argv[0]
    board = argv[1]
    print('---', method, board)
    initState(board)
    start = time.time()
    res = False
    if method == 'bfs':
        res = breadthFirstSearch()
    elif method == 'dfs':
        res = depthFirstSearch()
    elif method == 'dls':
        res = depthLimitedSearch(3)
    elif method == 'ids':
        res = iterativeDeepeningSearch()
    elif method == 'ast':
        res = aStarSearch()
    elif method == 'ida':
        res = idaSearch()

    print('result:', res)

if __name__ == "__main__":
   main(sys.argv[1:])
   # 3,1,2,0,4,5,6,7,8
   # 1,2,5,3,4,0,6,7,8
   # 3,1,2,4,0,5,6,7,8
   # 8,7,6,5,4,3,2,1,0
   # 3,1,2,4,5,8,0,6,7
   # 1,2,5,3,4,0,6,7,8,9,10,11,12,13,14,15
   # 1,2,0,3,4,5,6,7,8,9,10,11,12,13,14,15
   # 4,1,2,3,8,5,6,7,0,9,10,11,12,13,14,15
   # 4,1,2,3,8,5,6,7,9,10,0,11,12,13,14,15