#include <queue>
#include <stack>
#include <set>
#include <list>
#include <iostream>
using namespace std;

namespace ai {

    template <class TState, class TAction>
    class StateNode {
    public:
        virtual ~StateNode() {}
        StateNode* parentStateNode;
        TState state;
        TAction action;
        int depth;

        StateNode() : parentStateNode(NULL), depth(0) {}

        virtual bool isEqual(StateNode<TState, TAction>* stateNode) {
            return state == stateNode->state;
        }

        virtual list<StateNode<TState, TAction>*> successors() = 0;
    };

    template<class TState, class TAction>
    class BreadthFirstSearch {
    public:

        ~BreadthFirstSearch() {
            for (auto it = removed.begin(); it != removed.end(); ++it)
                delete *it;
            removed.clear();
            frontierSet.clear();
            explored.clear();
            queue<StateNode<TState, TAction>*> empty;
            swap(frontier, empty);
        }

        StateNode<TState, TAction>* initialStateNode;
        StateNode<TState, TAction>* goalStateNode;

    protected:
        StateNode<TState, TAction>* foundStateNode;
        queue<StateNode<TState, TAction>*> frontier;
        set<StateNode<TState, TAction>*> removed;
        set<TState> frontierSet;
        set<TState> explored;

    public:
        bool search() {
            frontier.push(initialStateNode);
            frontierSet.insert(initialStateNode->state);

            while (!frontier.empty()) {
                StateNode<TState, TAction>* stateNode = frontier.front();
                frontier.pop();
                frontierSet.erase(stateNode->state);
                explored.insert(stateNode->state);

                if (isGoalState(stateNode))
                    return true;

                for (auto node: stateNode->successors()) {
                    removed.insert(node);
                    if (frontierSet.find(node->state) == frontierSet.end()) {
                        if (explored.find(node->state) == explored.end()) {
                            frontier.push(node);
                            frontierSet.insert(node->state);
                        }
                    }
                }
            }

            return false;
        }

        bool isGoalState(StateNode<TState, TAction>* stateNode) {
            if (goalStateNode->isEqual(stateNode)) {
                foundStateNode = stateNode;
                return true;
            }
            else
                return false;
        }

        list<TAction> actions() {
            list<TAction> actions;
            StateNode<TState, TAction>* node = foundStateNode;
            actions.push_front(node->action);
            while (node->parentStateNode) {
                node = node->parentStateNode;
                if (node->parentStateNode)
                    actions.push_front(node->action);
            }
            return actions;
        }
    };

    template<class TState, class TAction>
    class DepthFirstSearch {
    public:

        ~DepthFirstSearch() {
            for (auto it = removed.begin(); it != removed.end(); ++it)
                delete *it;
            removed.clear();
            frontierSet.clear();
            explored.clear();
            stack<StateNode<TState, TAction>*> empty;
            swap(frontier, empty);
        }

        StateNode<TState, TAction>* initialStateNode;
        StateNode<TState, TAction>* goalStateNode;

    protected:
        StateNode<TState, TAction>* foundStateNode;
        stack<StateNode<TState, TAction>*> frontier;
        set<StateNode<TState, TAction>*> removed;
        set<TState> frontierSet;
        set<TState> explored;

    public:
        bool search() {
            frontier.push(initialStateNode);
            frontierSet.insert(initialStateNode->state);

            while (!frontier.empty()) {
                StateNode<TState, TAction>* stateNode = frontier.top();
                frontier.pop();
                frontierSet.erase(stateNode->state);
                explored.insert(stateNode->state);

                if (isGoalState(stateNode))
                    return true;

                list<StateNode<TState, TAction>*> successors = stateNode->successors();
                successors.reverse();
                for (auto node: successors) {
                    removed.insert(node);
                    if (frontierSet.find(node->state) == frontierSet.end()) {
                        if (explored.find(node->state) == explored.end()) {
                            frontier.push(node);
                            frontierSet.insert(node->state);
                        }
                    }
                }
            }

            return false;
        }

        bool isGoalState(StateNode<TState, TAction>* stateNode) {
            if (goalStateNode->isEqual(stateNode)) {
                foundStateNode = stateNode;
                return true;
            }
            else
                return false;
        }

        list<TAction> actions() {
            list<TAction> actions;
            StateNode<TState, TAction>* node = foundStateNode;
            actions.push_front(node->action);
            while (node->parentStateNode) {
                node = node->parentStateNode;
                if (node->parentStateNode)
                    actions.push_front(node->action);
            }
            return actions;
        }
    };

    template<class TState, class TAction>
    class DepthLimitedSearch {
    public:

        ~DepthLimitedSearch() {
            for (auto it = removed.begin(); it != removed.end(); ++it)
                delete *it;
            removed.clear();
            frontierSet.clear();
            explored.clear();
            stack<StateNode<TState, TAction>*> empty;
            swap(frontier, empty);
        }

        StateNode<TState, TAction>* initialStateNode;
        StateNode<TState, TAction>* goalStateNode;
        int depth;

    protected:
        StateNode<TState, TAction>* foundStateNode;
        stack<StateNode<TState, TAction>*> frontier;
        set<StateNode<TState, TAction>*> removed;
        set<TState> frontierSet;
        set<TState> explored;

    public:
        bool search() {
            frontier.push(initialStateNode);
            frontierSet.insert(initialStateNode->state);

            while (!frontier.empty()) {
                StateNode<TState, TAction>* stateNode = frontier.top();
                frontier.pop();

                if (stateNode->depth > depth)
                    continue;

                frontierSet.erase(stateNode->state);
                explored.insert(stateNode->state);

                if (isGoalState(stateNode))
                    return true;

                list<StateNode<TState, TAction>*> successors = stateNode->successors();
                successors.reverse();
                for (auto node: successors) {
                    removed.insert(node);
                    node->depth = stateNode->depth + 1;
                    if (frontierSet.find(node->state) == frontierSet.end()) {
                        if (explored.find(node->state) == explored.end()) {
                            frontier.push(node);
                            frontierSet.insert(node->state);
                        }
                    }
                }
            }

            return false;
        }

        bool isGoalState(StateNode<TState, TAction>* stateNode) {
            if (goalStateNode->isEqual(stateNode)) {
                foundStateNode = stateNode;
                return true;
            }
            else
                return false;
        }

        list<TAction> actions() {
            list<TAction> actions;
            StateNode<TState, TAction>* node = foundStateNode;
            actions.push_front(node->action);
            while (node->parentStateNode) {
                node = node->parentStateNode;
                if (node->parentStateNode)
                    actions.push_front(node->action);
            }
            return actions;
        }
    };

    template<class TState, class TAction>
    class IterativeDeepeningSearch {
    public:
        IterativeDeepeningSearch() : depth(0) {}

        StateNode<TState, TAction>* initialStateNode;
        StateNode<TState, TAction>* goalStateNode;

    protected:
        int depth;
        list<TAction> listActions;

    public:
        bool search() {
            while (true) {
                DepthLimitedSearch<TState, TAction> dls;
                dls.initialStateNode = initialStateNode;
                dls.goalStateNode = goalStateNode;
                dls.depth = depth;
                if (dls.search()) {
                    listActions = dls.actions();
                    return true;
                }
                depth++;
            }

            return false;
        }

        list<TAction> actions() {
            return listActions;
        }
    };
}
